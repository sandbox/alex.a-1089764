<?php

// $Id$

/**
 * @file
 * The petali admin page for configuring the module. 
 */

function petali_admin_form() {
  $form = array();
  $form['petali_team_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Team Content-Type Name'),
    '#default_value' => variable_get( 'petali_team_type', FALSE),
    '#maxlength' => 60,
    '#description' => t('The name of the Content-Type that defines the teams.'),
    '#after_build' => array('petali_check_content_type'),
  );
  $form['petali_team_categories'] = array(
    '#type' => 'textfield',
    '#title' => t('Team Groups'),
    '#default_value' => variable_get( 'petali_team_categories', FALSE),
    '#maxlength' => 60,
    '#description' => t('A comma-separated list of field names of the team fields that define team categories.'),
    '#after_build' => array('petali_check_team_categories'),
  );
  $form['petali_start_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Contest Start Date'),
    '#default_value' => variable_get( 'petali_start_date', FALSE),
    '#maxlength' => 20,
    '#description' => t('The first date of the contest.  (DD-MM-YYYY)'),
  );
  $form['petali_end_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Contest End Date'),
    '#default_value' => variable_get( 'petali_end_date', FALSE),
    '#maxlength' => 20,
    '#description' => t('The last date of the contest.  (DD-MM-YYYY)'),
    );
  $form['petali_close_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Contest Closing Date'),
    '#default_value' => variable_get( 'petali_close_date', FALSE),
    '#maxlength' => 20,
    '#description' => t('The last date to enter data.  (DD-MM-YYYY)'),
    );
    $form['petali_team_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum Team Size'),
    '#default_value' => variable_get( 'petali_team_size', 10),
    '#maxlength' => 20,
    '#description' => t('The maximum # of users in a team.'),
    '#after_build' => array('petali_check_size'),
    );
    $form['petali_distance_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum Trip Distance'),
    '#default_value' => variable_get( 'petali_distance_limit', 100),
    '#maxlength' => 20,
    '#description' => t('The maximum allowed trip distance.'),
    '#after_build' => array('petali_check_float'),
    );
    $form['petali_gasoline_consumption'] = array(
    '#type' => 'textfield',
    '#title' => t('Mean Gasoline Consumption(L/100 Km)'),
    '#default_value' => variable_get( 'petali_gasoline_consumption', 10),
    '#maxlength' => 20,
    '#description' => t('Coefficient for converting distance to amount of gasoline'),
    '#after_build' => array('petali_check_float'),
    );
    $form['petali_form_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Log Form Text'),
    '#default_value' => variable_get( 'petali_form_header', FALSE),
    '#description' => t('Text for the log form page.'),
    '#rows' => 5,
    );
  $form['petali_report_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Report Text'),
    '#default_value' => variable_get( 'petali_report_text', FALSE),
    '#description' => t('Text for the total report.'),
    '#rows' => 5,
    );
    return system_settings_form($form);
}

function _petali_verify_field($form_element, $content_type, $field_name, $field_type) {
  $field = content_fields($field_name, $content_type);
  $error = FALSE;
  $error_params = array('%content_type' => $content_type, '%field' => $field_name, '%field_type' => $field_type);
  if (!$field) {
    $error = t('Content Type %content_type does not have field %field', $error_params);
  }
  elseif ($field['type'] != $field_type) {
    $error = t('Field %field does not have the right type (%field_type)', $error_params);
  }
  elseif ($field['multiple'] != 0) {
    $error = t('Field %field should be single-valued', $error_params);
  }
  if ($error) {
    $form_item = $form_element['#parents'][0];
    form_set_error($form_item, $error);
  }
}

function petali_check_content_type($form_element) {
  $type = $form_element['#value'];
  _petali_verify_field($form_element, $type, "field_petali_distance", 'number_float');
  _petali_verify_field($form_element, $type, "field_petali_round_trips", 'number_float');
  _petali_verify_field($form_element, $type, "field_petali_riders", 'number_integer');
  _petali_verify_field($form_element, $type, "field_petali_size", 'number_integer');
  return $form_element; 
}

function petali_check_team_categories($form_element) {
  $type = $form_element['#value'];
  return $form_element; 
}

function petali_check_size($form_element) {
  $size = $form_element['#value'];
  if ( ! is_numeric($size)) {
    $form_item = $form_element['#parents'][0];
    form_set_error($form_item, t('Team size is not an integer: %size.', array('%size' => $size)));
  } 
  return $form_element;
}

function petali_check_float($form_element) {
  $value = $form_element['#value'];
  if ( ! is_numeric($value)) {
    $form_item = $form_element['#parents'][0];
    form_set_error($form_item, t('Not a number: %value.', array('%value' => $value)));
  } 
  return $form_element;
}


