Drupal Module to keep track of users' bicycle commute kilometers.
To be used for a bike-to-work commute challenge.

Petali defines two database table: petali_record, petali_team

It defines the pages:
* petali
  	Displays a form for the current user to enter/edit commute information for the current week.
* petali/report
	Displays summary commute information for all users.
* admin/settings/petali
	Administration screen for defining configuration parameters.
	
It is used together with a content-type and a view which are provided in the source but not installed automatically.
* petali
	Content-type that defines the teams.
	May be changed to any name as long as the view (and the petali admin settings are changed accordingly).
* petali_teams
	View that displays bike to work teams and their commute data.

Copyright 2010, 2011 Alexandros Athanasopoulos
Written for www.podilates.gr
Ποδήλατα στους δρόμους, στα τρένα, στο μετρό!
Bicycles on streets, in trains, in the subway.
 
