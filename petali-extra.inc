<?php

// $Id$

module_load_include('inc', 'petali');          

/**
 * Clear all entries for a user
 * @param user $user
 * @return page
 */
function petali_clear_user($user) {
  $p = petali_parameters();
  if ($p->start_date > 0 && $p->start_date < $p->end_date) {
    watchdog('actions', 'cleared entries for user %uid (%name)',
      array('%uid' => $user->uid,
       '%name' => $user->name));    
    $q = db_query( 'DELETE FROM {petali_record}' .
    ' WHERE uid = %d AND entry_date >= %d AND entry_date <= %d',
    array( $user->uid, $p->start_date, $p->end_date ) );
  }
  drupal_goto("petali/user/$user->uid");
}
