<?php

// $Id$

/**
 * @file
 * The petali computation and UI code. 
 */

define(PETALI_CO2_PER_L, 2.321); // Kg of CO2 per L of gasoline

class PetaliSummary {
  public $start_date;
  public $end_date;
  public $computed_timestamp;
  public $days;
  public $round_trips;
  public $distance;
  public $users;
  public $user_count;
  public $team_count;  
  function __construct() {
    $p = petali_parameters();
    $this->start_date = $p->start_date;
    $this->end_date = $p->end_date;
    $this->computed_timestamp = time();
    $this->days = 0;
    $this->round_trips = 0;
    $this->distance = 0;
    $this->team_count = 0;
    $this->user_count = 0;
    $this->users = array();
  }
}

class PetaliParameters {
  public  $start_date;
  public  $end_date;
  public  $team_type;
  public  $team_size;
}

class PetaliRecord {
  public  $trip_count;
  public  $distance;
  public  $entry_date;
}

/**
 * Get the administration computation parameters
 */
function petali_parameters() {
  static  $p;
  static  $valid;
  if (!$p) {
    $p = new PetaliParameters();
    $p->start_date = _petali_parse_date(variable_get('petali_start_date', FALSE)); 
    $p->end_date   = _petali_parse_date(variable_get('petali_end_date', FALSE)); 
    $p->team_type   = variable_get( 'petali_team_type', FALSE);
    $p->team_size = variable_get( 'petali_team_size', 10);
    $valid = $p->start_date && $p->end_date && $p->team_type;
  }  
  return $valid ? $p : FALSE;
}

/** Load records between two dates (inclusive) for the current user.
 * @param $start_date
 * @param $end_date
 * @return an array of objects with properties the names of the columns in the records table
 */
function petali_load( $uid, $start_date, $end_date ) {
  $result = array();
  $q = db_query( 'select entry_date, trip_count, distance from {petali_record}' .
    ' where uid = %d and entry_date >= %d and entry_date <= %d',
    array( $uid, $start_date, $end_date ) );
  while ($r = db_fetch_object($q)) {
    $result[] = $r;
  }    
  return $result;
}

/** Delete records between two dates (inclusive) for the current user. */
function petali_delete( $start_date, $end_date ) {
  global $user;
  db_query( 'DELETE FROM {petali_record}' .
  ' WHERE uid = %d AND entry_date >= %d AND entry_date <= %d',
  array( $user->uid, $start_date, $end_date ) );
}

/** Add a record
 * @param $entry_date The entry date
 * @param $trip_count The number of trips, should be 1 or 2
 * @param $distance The distance in km
 * @return nothing
 */
function petali_add( $entry_date, $trip_count, $distance ) {
  global $user;
  db_query( 'INSERT INTO {petali_record} (uid,entry_date,trip_count,distance)' .
  ' VALUES(%d,%d,%d,%f)',
  array( $user->uid, $entry_date, $trip_count, $distance ));
}

/**
 * Get a record for the given date, by searching an array of records.
 * If the record is not found, return an empty record.
 * @param $records
 * @param $date
 * @return an object with properties entry_date, trip_count, distance
 */
function petali_get( $records, $date ) {  
  foreach ($records as $r) {
    if ($r->entry_date == $date) {
      return $r;
    }
  }
  $r = new stdClass();
  $r->entry_date = $date;
  $r->trip_count = 0;
  $r->distance = 0;
  return $r;
}

/** Convert a unix timestamp to an entry_date. */
function petali_date_id( $time ) {
  return 0 + date( 'Ymd', $time );
}

/** Convert an entry_date to a unix timestamp. */
function petali_mktime( $date_id ) {
  $ymd = petali_explode_date( $date_id );
  return mktime( 12, 0, 0, $ymd[1], $ymd[2], $ymd[0] ); 
}

/** Convert an entry date to an array of its components (year, month, day) */
function petali_explode_date( $date_id ) {
  $year = (int) ($date_id / 10000);
  $month = ($date_id / 100) % 100;
  $day = $date_id % 100;
  return array( $year, $month, $day );
}

/** Create an entry date from its components */
function petali_implode_date( $year, $month, $day ) {
  return $year * 10000 + $month * 100 + $day;
}

function petali_format_date_id( $date_id) {
  return date( 'd M Y', petali_mktime($date_id));
}

function petali_date($time) {
  $days = array(
    t('Sunday'),
    t('Monday'),
    t('Tuesday'),
    t('Wednesday'),
    t('Thursday'),
    t('Friday'),
    t('Saturday'),
    );
  $fields = getdate($time);
  return date( 'd-m-Y ', $time) . ' - ' . $days[$fields['wday']];    
}

function petali_format_distance($d) {
  return sprintf( "%.2f", $d);  
}
/**
 * The form for editing a week's worth of data.
 * @param unknown_type $form_state
 * @param int $week The week to edit (0 or null: this week, 1: the previous week, ...)
 * @return unknown_type
 */
function petali_form($form_state, $week) {
  global $user;
  if ( ! $user || ! $user->uid ) {
    return FALSE;
  }
  $week = (int) $week;
  if ( $week < 0 ) {
    form_set_error('petali_form', 'Cannot edit future weeks.');
    return FALSE;
  }
  if ( $week > 5 ) {
    form_set_error('petali_form', 'Cannot go back that far.');
    return FALSE;
  }
  $links = array();
  if ( $week < 4 ) {
    $links[] = l( "(" . t('Previous Week') . ")", "petali/" . ($week + 1));
  }
  if ( $week > 0 ) {
    $links[] = l( "(" . t('Next Week') . ")", "petali/" . ($week - 1));
  }
  $link = implode( ' ', $links);
  
  $p = petali_parameters();
  $end_timestamp = petali_mktime($p->end_date);
  $now = time();
  if ( $now > $end_timestamp )
    $now = $end_timestamp;
  $now -= (24*3600*7*$week);
  $i = 0;
  $dateinfo = getdate($now);
  $weekday = $dateinfo['wday']; // 0 = Sunday
  $weekday1 = $weekday-1;
  if ( $weekday1 < 0 )
    $weekday1 += 7;
  // weekday1: 0 = Monday, 6 = Sunday
  
  $start_date = 0 + petali_date_id( $now + (24*3600*(0-$weekday1)) );
  $end_date = 0 + petali_date_id( $now + (24*3600*(7-$weekday1)) );
  $records = petali_load( $user->uid, $start_date, $end_date );
  $form['start_date'] = array(
    '#type' => 'hidden',
    '#value' => $start_date,
  );
  $form['header'] = array(  
    '#value' => ' ',
    '#suffix' => '<table><tr><th>' . t('Date') . '</th><th>' . t('Trips') . '</th><th>' . t('Distance (Km)') . '</th></tr><tr>',
  );
  // Display the current week, from Monday to Sunday   
  for ($i = 0; $i < 7; $i++) {
    $date_id = 0 + date( 'Ymd', $now + (24*3600*($i-$weekday1)) );
    if ($date_id > $p->end_date) {
      break;
    }
    if ($date_id < $p->start_date) {
      continue;
    }
    $date_label = petali_date( $now + (24*3600*($i-$weekday1)) );  
    
    $r = petali_get( $records, $date_id );
    $class = ($i % 2) ? 'even' : 'odd';
    $form["date_$date_id"] = array(
      '#value' => $date_label,
      '#prefix' => '<tr class="' . $class . '"><td>',
      '#suffix' => '</td>',
    );
    $form["trips_$date_id"] = array(
      '#type' => 'select',
      '#options' => array(
        '0' => '0',
        '2' => t('Round Trip'),
        '1' => t('To Work'),
        '-1' => t('From Work'),
    ),
      '#value' => $r->trip_count,        
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
    $form["distance_$date_id"] = array(
      '#type' => 'textfield',
      '#size' => 6,
      '#default_value' => petali_format_distance($r->distance),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#prefix' => '</table>',
  );
  $form['link'] = array(  
    '#value' => $link . ' ',
  );
  return $form;
}

/**
 * Handle the submission of the form.
 * Replace the week's data with the form's data.
 * @param $form
 * @param $form_state
 * @return unknown_type
 */
function petali_form_submit($form, &$form_state) {
  global $user;
  $values = $form['#post'];  
  $start_date = $values['start_date'];
  if ( ! $start_date ) {
    form_set_error('petali_form', 'Invalid Submission.');
    return;
  }
  $start_time = petali_mktime( $start_date );
  $end_date = petali_date_id( $start_time + 6 * 24 * 3600 );
  $entries = array();
  $distance_limit =  (float) variable_get( 'petali_distance_limit', 0);
  for ($i = 0; $i < 7; $i++) {    
    $date_id = petali_date_id( $start_time + $i * 24 * 3600 );
    $trip_count = $values["trips_$date_id"];
    $distance = $values["distance_$date_id"];
    $distance = str_replace( ',', '.', $distance ); // use either form of decimal points
    $distance = (float) $distance; 
    if ( $trip_count && $distance ) {
      if ($distance_limit && $distance > $distance_limit) {
        form_set_error("distance_$date_id", 'Daily distance exceeds limit.');
        return;        
      }
      $entry = new PetaliRecord();
      $entry->trip_count = $trip_count;
      $entry->distance = $distance;
      $entry->date_id = $date_id;
      $entries[] = $entry; 
    }
  }  
  
  petali_delete( $start_date, $end_date );
  foreach($entries as $entry) {
    if ($entry->trip_count && $entry->distance) {
      petali_add( $entry->date_id, $entry->trip_count, $entry->distance );
    }
  }  
  drupal_set_message(t('Entry accepted.  Thank you.'), 'status');
}

/**
 * Return the table of summary data for the current user.
 * @param $summary The summary object for the current user.
 */
function petali_user_summary_table($user, $summary) {
  $p = petali_parameters();
  $team = FALSE;
  $q = db_query("SELECT n.nid, n.title FROM {node} AS n INNER JOIN {petali_team} AS t ON t.nid = n.nid AND t.uid = %d AND t.team_type = '%s'",
    $user->uid, $p->team_type);
  if ( $r = db_fetch_object($q) ) {
    $team = l($r->title, "node/$r->nid");
  }
  $rows = array();
  $rows[] = array( t('Days'), $summary->days);
  $rows[] = array( t('Round Trips'), $summary->round_trips);
  $rows[] = array( t('Distance (km)'), petali_format_distance($summary->distance));
  $rows[] = array( t('Start Date'), petali_format_date_id($summary->start_date));
  $rows[] = array( t('End Date'), petali_format_date_id($summary->end_date));  
  $rows[] = array( t('Team'), $team);  
  $header = t('Total for %user', array( '%user' => $user->name));
  return theme_table( NULL, $rows, array(), "<strong>$header</strong>" );    
}

/**
 * Implementation of the commute data entry form.
 * @param int $week Determines the week to display the form for.
 * 0 (or NULL) is the current week.  1 is the previous week, etc.
 */
function petali_page($week = NULL) {
  global $user;
  if ( ! $user || ! $user->uid ) {
    drupal_set_message(t('Please Login.'), 'error');
    return FALSE;
  }
  if ( $week && !is_numeric($week) ) {
    return drupal_access_denied();    
  }
  
  $user_summary = petali_compute_user_summary($user->uid);  
  $report = petali_user_summary_table($user, $user_summary);
  $close_date = _petali_parse_date(variable_get('petali_close_date', FALSE)); 
  if (!$close_date || time() < petali_mktime($close_date)) {
    $user_info = t('Rider: %user', array('%user' => $user->name));  
    $form = $user_info . drupal_get_form( 'petali_form', $week );
  } else {
    $form = t('Trip entry has closed.' . '<hr>');
  }
  $text = variable_get('petali_form_header', FALSE);
  return $form . $text . $report;
}

/**
 * The total statistics report page.
 */
function petali_report_page() {
  $summary = petali_load_summary();
  $text = variable_get( 'petali_report_text', FALSE);
  if (!$summary) {
    return NULL;
  }
  return petali_summary_table($summary) . $text;
}

/** Convert a date of the form D-M-Y to a datenumber of the form YYYYDDMM
 * @param $s
 * @return int
 */
function _petali_parse_date($s) {
  if (!$s) {
    return FALSE;
  }
  $fields = explode( '-', $s);
  if (count($fields) != 3) {
    return FALSE;
  }
  return petali_implode_date( $fields[2], $fields[1], $fields[0] );
}

/** Compute the summary data for a user.
 * @param $user the user for which to compute the summary.
 * @return a PetaliSummary
 */
function petali_compute_user_summary($uid ) {
  $p = petali_parameters();
  if (!p) {
    return FALSE;
  }
  $query = 'SELECT uid, entry_date, trip_count, distance FROM {petali_record}' .
    ' WHERE uid = %d AND entry_date >= %d AND entry_date <= %d';
  $q = db_query( $query, $uid, $p->start_date, $p->end_date );
  $summary = new PetaliSummary();
  while ($r = db_fetch_object($q)) {
    $summary->days++;
    $summary->round_trips += abs($r->trip_count) / 2;
    $summary->distance += $r->distance;
  }    
  $summary->user_count = 1;
  return $summary;
}


/** Compute the summary between two dates
 * @param $start_date  The start date, as a date stamp.
 * @param $end_date    The end date, as a date stamp.
 * @return a PetaliSummary
 */
function petali_compute_summary_with_teams() {
  $p = petali_parameters();
  if (!p) {
    return FALSE;
  }
  $query = 'SELECT p.uid, p.entry_date, p.trip_count, p.distance, t.nid FROM {petali_record} AS p' .
    ' LEFT JOIN {petali_team} AS t on t.uid = p.uid' .
    " WHERE (t.team_type = '%s' OR t.nid is null) AND entry_date >= %d AND entry_date <= %d";

  $end_date = petali_date_id(time());
  if ($p->end_date < $end_date) {
    $end_date = $p->end_date;
  } 
  
  $params = array( $p->team_type, $p->start_date, $end_date );
  $q = db_query( $query, $params );
  $teams = array();
  $users = array();
  $summary = new PetaliSummary();
  $summary->start_date = $p->start_date;
  $summary->end_date = $p->end_date;
  while ($r = db_fetch_object($q)) {
    $r->trip_count = abs($r->trip_count);
    $summary->days++;
    $summary->round_trips += $r->trip_count / 2;
    $summary->distance += $r->distance;
    $users[$r->uid] = TRUE;
    $nid = $r->nid;
    if ($nid) {
      $team = $teams[$nid];
      if (!$team) {
        $team = new PetaliSummary();
        $teams[$nid] = $team;
      }
      $team->days++;
      $team->round_trips += $r->trip_count / 2;
      $team->distance += $r->distance;
      $team->users[$r->uid] = TRUE;
    }
  }    
  $summary->user_count = count($users);
  $summary->team_count = count($teams);
  foreach ($teams as $nid => $team) {
    $team->user_count = count($team->users);
  }
  petali_save_teams($p->team_type, $teams);
  petali_save_summary($summary);
  $categories = explode(',', variable_get( 'petali_team_categories', ''));
  foreach ($categories as $category) {
    petali_save_team_category($p->team_type, $teams, $category);
  }
  return $summary;
}

/**
 * Set a cck field to 0 for all nodes of a given content type
 * @param $field_name  The name of the field
 * @param $team_type   The content type
 */
function petali_zero_field($field_name, $team_type) {
  $field_info = content_database_info(content_fields($field_name, $team_type));
  $table = $field_info['table'];
  $column = $field_info['columns']['value']['column'];
  if (!$column) {
    return;
  }
  
  // zero existing rows
  db_query( "UPDATE {$table} AS f INNER JOIN node AS n ON f.vid = n.vid SET f.$column = 0 WHERE n.type = '%s'",
    $team_type);
    
  // insert missing rows
  $q = db_query( "SELECT n.nid, n.vid FROM {node} AS n WHERE n.type = '%s'" .
    " AND NOT EXISTS (SELECT * FROM {$table} WHERE vid = n.vid)",
    $team_type);
  while ($r=db_fetch_object($q)) {
    db_query( "INSERT INTO {$table} (nid,vid,$column) VALUES(%d,%d,0)",
      $r->nid, $r->vid);    
  }
}

/**
 * Set a cck numeric field value.  Assumes the field already has a value.
 * @param array $field_info  The field information, as returned from content_database_info
 * @param int $nid  The node id
 * @param number $value  The value to set
 */
function petali_set_value( $field_info, $nid, $value) {
  $table = $field_info['table'];
  $column = $field_info['columns']['value']['column'];
  if ( !$column) {
    return;
  }
  $type = $field_info['columns']['value']['type'];
  $format = FALSE;
  if ($type == 'int' || $type == 'number_integer') {
    $format = '%d';
  }
  elseif ($type == 'float' || $type == 'number_float') {
    $format = '%f';
  }
  if ($format) {
    $sql = "UPDATE {$table} AS f INNER JOIN node AS n ON f.vid = n.vid SET f.$column = $format WHERE n.nid = %d";
    db_query($sql, $value, $nid);
  }
} 

/**
 * Update team fields with computed values.
 * @param string $team_type The team content type
 * @param An array of team objects, indexed by node id.
 */
function petali_save_teams($team_type, $teams) {
  petali_zero_field('field_petali_riders', $team_type);
  petali_zero_field('field_petali_distance', $team_type);
  petali_zero_field('field_petali_round_trips', $team_type);
  petali_zero_field('field_petali_teams', $team_type);
  
  $riders_info = content_database_info(content_fields('field_petali_riders', $team_type));
  $distance_info = content_database_info(content_fields('field_petali_distance', $team_type));
  $round_trips_info = content_database_info(content_fields('field_petali_round_trips', $team_type));
  $teams_info = content_database_info(content_fields('field_petali_teams', $team_type));
  // now fill in the data for the teams that have data
  foreach ($teams as $nid => $team) {
    petali_set_value( $riders_info, $nid, $team->user_count);
    petali_set_value( $distance_info, $nid, $team->distance);
    petali_set_value( $round_trips_info, $nid, $team->round_trips);
    petali_set_value( $teams_info, $nid, $team->team_count);
  }
}

/**
 * Get a map of nid => nid from team to category
 * @param $team_type  The team content type
 * @param $field_info The field information for the field that defines the category, as returned by content_database_info.
 */
function petali_get_groups($team_type, $field_info) {
  $table = $field_info['table'];
  $column = $field_info['columns']['nid']['column'];
  $sql = "SELECT n.nid AS nid, f.$column AS field_value FROM {$table} AS f INNER JOIN node AS n ON f.vid = n.vid";
  $q = db_query($sql, $value, $nid);
  $map = array();
  while ( $r = db_fetch_object($q)) {
    $map[$r->nid] = $r->field_value;
  }
  return $map;
}

function petali_group_teams($teams, $team_groups) {
  $groups = array();
  foreach ($teams as $tid => $team) {
    $gid = $team_groups[$tid];
    if ($gid) {
      $group = $groups[$gid];
      if (!$group) {
        $group = new PetaliSummary();
        $groups[$gid] = $group;
      }
      $group->team_count++;
      $group->round_trips += $team->round_trips;
      $group->distance += $team->distance;
      $group->user_count += $team->user_count;      
    }
  }
  return $groups;
}

function petali_save_team_category($team_type, $teams, $team_field) {
  $content_fields = content_fields($team_field, $team_type);
  if ( $content_fields['type'] != 'nodereference') {
    return;
  }
  $group_types = array();
  foreach ($content_fields['referenceable_types'] as $type => $value) {
    if ($value) {
      $group_types[] = $type;
    }
  }
  if (count($group_types) != 1) {
    return;
  }
  $group_type = $group_types[0];
  $field_info = content_database_info($content_fields);
  $team_groups = petali_get_groups($team_type, $field_info);
  $groups = petali_group_teams($teams, $team_groups);  
  petali_save_teams($group_type, $groups);
}

/**
 * Construct the total summary table
 * @param object $summary  The summary data
 */
function petali_summary_table($summary) {
  $p = petali_parameters();
  $rows = array();
  $rows[] = array( t('Riders'), $summary->user_count);
  $rows[] = array( t('Distance (Km)'), petali_format_distance( $summary->distance));
  $rows[] = array( t('Bike Days'), $summary->days);
  $rows[] = array( t('Round Trips'), $summary->round_trips);
  if ($summary->round_trips) {
    $rows[] = array( t('Average Km per Trip'),
      petali_format_distance( $summary->distance / $summary->round_trips));    
  }
  $rows[] = array( t('Teams'), $summary->team_count);
  $gasoline = $summary->distance / 100.0 * variable_get( 'petali_gasoline_consumption', 10); 
  $rows[] = array( t('Equivalent gasoline (L)'), (int) $gasoline);
  $rows[] = array( t('Equivalent CO2 (Kg)'), (int) ($gasoline * PETALI_CO2_PER_L));
  $rows[] = array( t('Start Date'), petali_format_date_id($summary->start_date));
  $rows[] = array( t('End Date'), petali_format_date_id($summary->end_date));
  $rows[] = array( t('Computed On'), date( 'd-m-Y H:i', $summary->computed_timestamp));
  return theme_table( NULL, $rows, array());    
}

/** Load the cached summary and convert it into an object.
 */
function petali_load_summary() {
  $a = variable_get('petali_summary', NULL);
  if (!$a) {
    return FALSE;
  }
  $s = new PetaliSummary();
  $s->start_date = $a['start_date'];
  $s->end_date = $a['end_date'];
  $s->round_trips = $a['round_trips'];
  $s->days = $a['days'];
  $s->team_count = $a['team_count'];
  $s->user_count = $a['user_count'];
  $s->distance = $a['distance'];
  $s->computed_timestamp = $a['computed_timestamp'];
  return $s;
}

/** Save the total summary object into a variable.
 */
function petali_save_summary($summary) {
  $a = array();
  $a['start_date'] = $summary->start_date; 
  $a['end_date'] = $summary->end_date; 
  $a['computed_timestamp'] = $summary->computed_timestamp; 
  $a['days'] = $summary->days; 
  $a['round_trips'] = $summary->round_trips; 
  $a['distance'] = $summary->distance; 
  $a['user_count'] = $summary->user_count; 
  $a['team_count'] = $summary->team_count;
  variable_set('petali_summary', $a); 
}

/** Implementation of a page that performs the computation.
 *  Redirects to the summary report page.
 */
function petali_compute_page() {
  $summary = petali_compute();
  if (!$summary) {
    return t("Invalid Configuration");
  }
  drupal_goto("petali/report");
}

/**
 * Recompute team sizes and save them in the teams.
 * @return void
 */
function petali_compute_team_sizes() {
  $p = petali_parameters();
  $size_info = content_database_info(content_fields('field_petali_size', $p->team_type));
  petali_zero_field('field_petali_size', $p->team_type);  
  $q = db_query('SELECT nid, count(uid) AS size FROM {petali_team} GROUP BY nid');
  while ($r = db_fetch_object($q)) {
    petali_set_value( $size_info, $r->nid, $r->size);    
  }
}

/**
 * Performs the periodic computation of statistics, team, and all other computations.
 */
function petali_compute() {
  watchdog('actions', 'petali computed');
  petali_compute_team_sizes();
  $summary = petali_compute_summary_with_teams();
  cache_clear_all();  // clear page and block caches
  // cache_clear_all(NULL, 'cache_content');
  // cache_content uses permanent caching, so it does not get cleared with cache_clear_all, unless we use explicit cids
  db_query('DELETE FROM {cache_content}');
  return $summary;
}

/** Computes the number of members in a team. */
function petali_team_size($nid) {
  $p = petali_parameters();
  $sql = "SELECT COUNT(*) FROM {petali_team} WHERE nid = %d AND team_type = '%s'";
  return (int) db_result(db_query( $sql, $nid, $p->team_type));  
}

/**
 * Update the team size field of a team.
 * @param $nid
 */
function petali_update_team_size($nid) {
  if (!$nid) {
    return;
  }
  $p = petali_parameters();
  $team_size = petali_team_size($nid);
  $size_info = content_database_info(content_fields('field_petali_size', $p->team_type));
  petali_set_value( $size_info, $nid, $team_size);
}

/**
 * Implements the page that lets a user join a team.
 * Redirects to the team.
 * @param node $node The team node.
 */
function petali_join_team($node) {
  global $user;
  $p = petali_parameters();
  $team_size = petali_team_size($node->nid);
  if ($team_size >= $p->team_size) {
    return t('Team %name already has %size members.  Please join another team.',
      array('%name' => $node->title, '%size' => $team_size));    
  }
  $old_team = petali_get_team($user->uid);
  db_query( "DELETE FROM {petali_team} WHERE uid = %d AND team_type = '%s'", $user->uid, $p->team_type);
  db_query( "INSERT INTO {petali_team} (uid,team_type,nid) values(%d,'%s',%d)", $user->uid, $p->team_type, $node->nid);
  petali_update_team_size($old_team);  
  petali_update_team_size($node->nid);  
  drupal_goto("node/" . $node->nid);  
}

/**
 * Implements the page that lets a user leave a team (and therefore have no team).
 * Redirects to the team.
 * @param node $node The team node.
 */
function petali_leave_team($node) {
  global $user;
  $p = petali_parameters();
  db_query( "DELETE FROM {petali_team} WHERE uid = %d AND team_type = '%s' AND nid = %d", $user->uid, $p->team_type, $node->nid);
  petali_update_team_size($node->nid);  
  drupal_goto("node/" . $node->nid);  
}

/**
 * Return a table of team membership
 * @param int $nid The team node id
 */
function petali_team_members($team_nid) {
  $p = petali_parameters();
  $sql = 'SELECT u.uid AS uid, count(p.entry_date) AS days FROM {users} AS u' .
    ' INNER JOIN {petali_team} AS t ON t.uid = u.uid' .
    ' INNER JOIN {petali_record} AS p ON p.uid = u.uid' .
    ' WHERE t.nid = %d' .
    " AND p.entry_date >= $p->start_date AND p.entry_date <= $p->end_date" .
    ' GROUP BY u.uid';
  $q = db_query($sql, $team_nid);
  $user_entries = array();
  while ($r = db_fetch_object($q)) {
    $user_entries[$r->uid] = $r->days;
  }
  $q = db_query('SELECT u.uid AS uid, u.name AS name FROM {users} AS u' .
    ' INNER JOIN {petali_team} AS t ON t.uid = u.uid' .
    ' WHERE t.nid = %d' .
    ' ORDER BY name',
  $team_nid);
  $users = array();
  while ($r = db_fetch_object($q)) {
    $days = $user_entries[$r->uid];
    if (!$days) {
      $days = 0;
    }
    $users[] = array( l($r->name, "user/$r->uid"), $days);
  }
  $headers = array(t('Rider'), t('Days Logged'));
  return theme_table($headers, $users, array(), t('Team Members'));
}

/** Implementation of a page that shows largest entries.
 */
function petali_top_page($limit) {
  $p = petali_parameters();
  $rows = array(); 
  $query = 'SELECT r.entry_date, r.trip_count, r.distance, r.uid AS uid, u.name FROM {petali_record} AS r' .
    ' INNER JOIN {users} AS u ON r.uid = u.uid' .
    ' WHERE entry_date >= %d and entry_date <= %d AND distance > %d ORDER BY distance DESC';
  $args = array($p->start_date, $p->end_date, $limit );
  $q = db_query($query, $args);
  while ($r = db_fetch_object($q)) {
    $rows[] = array( $r->distance, $r->entry_date, $r->trip_count, l($r->name, "petali/user/$r->uid"));
  }
  $headers = array(t('Distance'), t('Date'), t('Trips'), t('User'));
  return theme_table($headers,$rows);
}

/**
 * Show entries for user
 * @param user $user
 * @return page
 */
function petali_user_page($user) {
  $p = petali_parameters();
  $rows = array(); 
    $q = db_query( 'select entry_date, trip_count, distance from {petali_record}' .
    ' where uid = %d and entry_date >= %d and entry_date <= %d order by entry_date',
    array( $user->uid, $p->start_date, $p->end_date ) );
  while ($r = db_fetch_object($q)) {
    $date = petali_date(petali_mktime($r->entry_date));
    $rows[] = array( $date, $r->trip_count, $r->distance );
  }
  $headers = array(t('Date'), t('Rides'), t('Distance'));
  $entries = theme_table( $headers, $rows, array());    
  $user_summary = petali_compute_user_summary($user->uid);  
  $report = petali_user_summary_table($user, $user_summary);
  return $report . $entries;
}



